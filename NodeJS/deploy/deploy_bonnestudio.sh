#!/bin/bash

tar czf pylonio.tar.gz app.js package.json public
scp pylonio.tar.gz demo@www.bonniestudio.com:~
rm pylonio.tar.gz

ssh demo@www.bonniestudio.com << 'ENDSSH'
pm2 delete pylon
rm -rf pylonio
mkdir pylonio
tar xf pylonio.tar.gz -C pylonio
rm pylonio.tar.gz
cd pylonio
npm install
pm2 start --name pylon app.js
ENDSSH
echo "Deploy Finished."

#pm2 start --name easy-1 app.js -- --name easy-1 --port 1337
#pm2 start --name easy-2 app.js -- --name easy-2 --port 1338