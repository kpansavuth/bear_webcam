
const express = require('express');
const app = express();

const sanitize = require('sanitize-filename');
const fileUpload = require('express-fileupload');
app.use(fileUpload());

app.use(express.static('public'))
// app.use(express.static(path.join(__dirname, 'public')));

app.post('/api/upload', function(req, res) {

    if (!req.files)
        return res.status(400).send('No files were uploaded.');
 
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let sampleFile = req.files.sampleFile;

    let fileName = sanitize( sampleFile.name );

    console.log("sampleFile: " + fileName);
    
    //Use the mv() method to place the file somewhere on your server
    sampleFile.mv( __dirname + '/public/uploaded/'+fileName, function(err) {
        if (err)
        {
            console.log( err );
            return res.status(500).send(err);
        }

        res.send('File '+ fileName + ' uploaded!');
        console.log( 'File '+ fileName + ' uploaded!' );

        const fs = require('fs');
        const moment = require('moment');
        // destination.txt will be created or overwritten by default.
        let _storedFilename = moment().format('YYYY-MM-DD_HH-mm-ss')+'.png';
        fs.copyFile('./public/uploaded/screenShot.png', './public/stored/'+_storedFilename, (err) => {
            if (err) throw err;
            console.log('source.txt was copied to destination.txt');
            
            //store gallery file            
            let _i = 8;
            let _oldpath = './public/gallery/img_0'+_i+'.png';
            let _newpath = './public/gallery/img_0'+(_i+1)+'.png';
            fs.copyFile(_oldpath, _newpath, (err)=>{
                --_i;
                _oldpath = './public/gallery/img_0'+_i+'.png';
                _newpath = './public/gallery/img_0'+(_i+1)+'.png';
                fs.copyFile(_oldpath, _newpath, (err)=>{
                    --_i;
                    _oldpath = './public/gallery/img_0'+_i+'.png';
                    _newpath = './public/gallery/img_0'+(_i+1)+'.png';
                    fs.copyFile(_oldpath, _newpath, (err)=>{
                        --_i;
                        _oldpath = './public/gallery/img_0'+_i+'.png';
                        _newpath = './public/gallery/img_0'+(_i+1)+'.png';
                        fs.copyFile(_oldpath, _newpath, (err)=>{
                            --_i;
                            _oldpath = './public/gallery/img_0'+_i+'.png';
                            _newpath = './public/gallery/img_0'+(_i+1)+'.png';
                            fs.copyFile(_oldpath, _newpath, (err)=>{
                                --_i;
                                _oldpath = './public/gallery/img_0'+_i+'.png';
                                _newpath = './public/gallery/img_0'+(_i+1)+'.png';
                                fs.copyFile(_oldpath, _newpath, (err)=>{
                                    --_i;
                                    _oldpath = './public/gallery/img_0'+_i+'.png';
                                    _newpath = './public/gallery/img_0'+(_i+1)+'.png';
                                    fs.copyFile(_oldpath, _newpath, (err)=>{
                                        --_i;
                                        _oldpath = './public/gallery/img_0'+_i+'.png';
                                        _newpath = './public/gallery/img_0'+(_i+1)+'.png';
                                        fs.copyFile(_oldpath, _newpath, (err)=>{
                                            --_i;
                                            _oldpath = './public/gallery/img_0'+_i+'.png';
                                            _newpath = './public/gallery/img_0'+(_i+1)+'.png';
                                            fs.copyFile(_oldpath, _newpath, (err)=>{                            
                                                fs.copyFile('./public/uploaded/screenShot.png', './public/gallery/img_00.png', (err) => {
                                                    if (err) throw err;
                                                    console.log('rename finished');
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                        
                    });
                });
            });

            //
        });

        

    }); 

});

app.listen(1340);