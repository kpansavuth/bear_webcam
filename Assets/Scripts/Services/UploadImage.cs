﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;

//using System.Xml;
//using System.Linq;
//using System.IO;
//using System.Text;
//using System.Net;

//Reference
//https://docs.unity3d.com/ScriptReference/WWWForm.html

namespace BonnieSDK {

	//Usage:
	//	UploadImage.Instance.Upload(
	//		new WWWForm (),
	//		"screenShot.png",
	//		"sampleFile",
	//		ctrl_capture.GetTexture2D(),
	//		"localhost:1340/api/upload",
	//		(bool _success) => {
	//			print( "update result: " + _success );
	//		}
	//	);

	public class UploadImage : Singleton<UploadImage> {

		public delegate void CallbackDelegate( bool _success );
		public void Upload( WWWForm _form, string _filename,  string _fieldName, Texture2D _tex, string _url, CallbackDelegate _callback = null ) {

			StartCoroutine (
				CoUploadImage (
					_form,
					_filename,
					_fieldName,
					_tex,
					_url,
					_callback
				)
			);

		}

		private IEnumerator CoUploadImage( WWWForm _form, string _filename, string _fieldName, Texture2D _tex, string _url, CallbackDelegate _callback ) {

			_form.AddBinaryData(_fieldName,
				ConvertTexture2DToByte(_tex), 
				_filename, 
				"image/png"
			);

			using (var _w = UnityEngine.Networking.UnityWebRequest.Post(_url, _form))
			{
				yield return _w.SendWebRequest();
				bool _isSuccess = !(_w.isNetworkError || _w.isHttpError);
				if (_isSuccess) {
					print("Finished Uploading Screenshot ");
				}
				else {
					print(_w.error);
				}
				if (_callback != null)
					_callback (_isSuccess);
			}

		}

		public static Byte[] ConvertTexture2DToByte( Texture2D _tex ) {
			return _tex.EncodeToPNG ();
		}

		public static string ConvertByteToBase64String( Byte []_bytes ) {
			return Convert.ToBase64String ( _bytes );
		}

	}

}


/* NodeJS Server
 
const express = require('express');
const app = express();

const sanitize = require('sanitize-filename');
const fileUpload = require('express-fileupload');
app.use(fileUpload());

app.post('/api/upload', function(req, res) {

    if (!req.files)
        return res.status(400).send('No files were uploaded.');
 
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let sampleFile = req.files.sampleFile;

    let fileName = sanitize( sampleFile.name );

    console.log("sampleFile: " + fileName);
    
    //Use the mv() method to place the file somewhere on your server
    sampleFile.mv( __dirname + '/public/uploaded/'+fileName, function(err) {
        if (err)
        {
            console.log( err );
            return res.status(500).send(err);
        }
    
        res.send('File '+ fileName + ' uploaded!');
        console.log( 'File '+ fileName + ' uploaded!' );
    }); 

});

app.listen(1340);

*/