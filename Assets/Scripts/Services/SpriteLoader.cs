﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace BonnieSDK {

	public class SpriteLoader {

		public static Sprite[] LoadSprite( string _path ) {

			Sprite []_sprites = Resources.LoadAll<Sprite>( _path );
			return _sprites;

		}

	}

}