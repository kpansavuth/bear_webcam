﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BonnieSDK {

	public class WriteConfigFile : MonoBehaviour {

		public static void WriteConfig( string _path, Dictionary<string,string> _data ) {

			List<string> _lines = new List<string> ();
			foreach (var _kvp in _data) {
				_lines.Add ( _kvp.Key + "=" + _kvp.Value );
			}
			System.IO.File.WriteAllLines ( Application.dataPath + "/" +  _path, _lines.ToArray ());


		}
	
	}

}