﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BonnieSDK {

	public class ReloadScene : Singleton<ReloadScene>
	{
	    public bool apply = false;

	    // Update is called once per frame
	    void Update()
	    {
	        if (apply)
	            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	    }

	    public void Reload()
	    {
	     	SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	    }

		public void ChangeScene( string _sceneName ){
			SceneManager.LoadScene (_sceneName);
		}
	}

}