﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustTimeScale : MonoBehaviour {

	public float targetTimeScale = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.F)) {
			Time.timeScale = targetTimeScale;
		} else {
			Time.timeScale = 1f;
		}
	}
}
