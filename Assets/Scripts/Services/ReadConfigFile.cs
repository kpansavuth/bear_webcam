﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BonnieSDK {

	public class ReadConfigFile : MonoBehaviour {

		//Path is relative to project's folder or build's folder
		//Assets
		//my_config.cfg
		public static List<string> ReadConfig (string _path)
		{

			List<string> _data = new List<string>();

			string[] _lines = System.IO.File.ReadAllLines ( Application.dataPath + "/" + _path);
			foreach (var _i in _lines) {

				//Ignore empty line
				if( string.Equals( _i, "" ) ) 
					continue;
				//Ignore comments
				if( _i[0] == '#' )
					continue;

				_data.Add( _i );

			}

			return _data;
		}

		public static Dictionary<string, string> ExtractKeyValuePair (List<string> _data, char _separator = '=')
		{

			var _kvp = new Dictionary<string, string> ();
			foreach (var _i in _data) {

				var _split = _i.Split (_separator);
				if (_split.Length == 2) {
					_kvp.Add( _split[0], _split[1] );
				}

			}
			return _kvp;

		}

	}

}