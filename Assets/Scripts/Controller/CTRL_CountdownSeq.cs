﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BonnieSDK;
using UnityEngine.UI;

public class CTRL_CountdownSeq : MonoBehaviour {

	public CTRL_CountdownUI ctrl_countdown;
	public CTRL_Capture ctrl_capture;
	public CTRL_BearAdjust ctrl_bear;
	public bool isPlayAtStart = false;
	private List<Sprite[]> spriteArrayList = new List<Sprite[]>();
	private List<float> animDudationList = new List<float> ();

	private float delay_before = 6f;
	private float delay_after = 5f;

	private float fps = 30f;

	public Image img_bg;
	public RawImage img_qr;
	public Image img_frame;
	public RawImage img_preview;
	public Image img_hideQR;

	private bool docapture = true;

	private string url_upload = "localhost:1340/api/upload";
	private string upload_filename = "screenShot.png";

	// Use this for initialization
	IEnumerator Start ()
	{
		var _kvp = ReadConfigFile.ExtractKeyValuePair (
			           ReadConfigFile.ReadConfig ("my_config.cfg")
		           );
		url_upload 		= _kvp ["URL_UPLOAD"];
		upload_filename = _kvp ["UPLOAD_FILENAME"];

		animDudationList.Clear ();
		animDudationList.Add (float.Parse (_kvp ["DURATION_ANIM1"]));
		animDudationList.Add (float.Parse (_kvp ["DURATION_ANIM2"]));
		animDudationList.Add (float.Parse (_kvp ["DURATION_ANIM3"]));

		delay_before 	= float.Parse (_kvp ["DELAY_BEFORE"]);
		delay_after 	= float.Parse (_kvp ["DELAY_AFTER"]);

		img_hideQR.gameObject.SetActive (int.Parse (_kvp ["HIDE_QR"]) != 0);
		docapture = (int.Parse (_kvp ["USE_CAPTURE"]) != 0);
		img_bg.gameObject.SetActive ((int.Parse (_kvp ["USE_BG"]) != 0));

		fps = float.Parse (_kvp ["FPS_COUNTDOWN"]);

		Sprite[] _sprites_countdown = SpriteLoader.LoadSprite (_kvp ["PATH_COUNTDOWN"]);
		Sprite[] _sprites_shutter = SpriteLoader.LoadSprite (_kvp ["PATH_SHUTTER"]);
		Sprite[] _sprites_preview = SpriteLoader.LoadSprite (_kvp ["PATH_PREVIEW"]);	
		spriteArrayList.Clear ();
		spriteArrayList.Add (_sprites_countdown);
		spriteArrayList.Add (_sprites_shutter);			
		spriteArrayList.Add (_sprites_preview);

		ctrl_countdown.img.enabled = false;
		img_qr.enabled = false;
		img_frame.enabled = false;
		img_preview.enabled = false;
		img_hideQR.enabled = false;
		ctrl_bear.m_animator.gameObject.SetActive (false);

		if (isPlayAtStart) {
			yield return new WaitForSeconds (3f);
			PlaySeq ();
//			TimerAlarm.LazyScheduleTimer (15f, () => {
//				PlaySeq ();
//			});
		}

	}
	
	// Update is called once per frame
//	void Update ()
//	{
//		if (Input.GetMouseButtonDown (0)) 			
//			PlaySeq();
//		
//	}

	public void PlaySeq() {
		StopAllCoroutines();
		if (docapture) {
			StartCoroutine (
				CoPlaySequence (
					spriteArrayList,
					ctrl_countdown,
					fps
				)
			);
		}
		StartCoroutine (
			CoBearSeq ()
		);
	}

	private IEnumerator CoBearSeq() {
		ctrl_bear.m_animator.gameObject.SetActive (true);

		ctrl_bear.m_animator.Play ("Anim1");
		yield return new WaitForSeconds( animDudationList[0] );
		ctrl_bear.m_animator.Play ("Anim3");
		yield return new WaitForSeconds( animDudationList[2] );
		ctrl_bear.m_animator.Play ("Anim1");
		yield return new WaitForSeconds( animDudationList[0] );
		ctrl_bear.m_animator.Play ("Anim3");
		yield return new WaitForSeconds( animDudationList[2] );
		ctrl_bear.m_animator.Play ("Anim1");
		yield return new WaitForSeconds( animDudationList[0] );
		ctrl_bear.m_animator.Play ("Anim3");
		yield return new WaitForSeconds( animDudationList[2] );

		if (!docapture) {
			StartCoroutine (
				CoBearSeq ()
			);
		}
	}

	private IEnumerator CoPlaySequence (List<Sprite[]> _spriteArrayList, CTRL_CountdownUI _ctrl_countdown, float _fps)
	{

		yield return new WaitForSeconds( delay_before );

		ctrl_countdown.img.enabled = true;
		img_qr.enabled = false;
		img_frame.enabled = false;
		img_preview.enabled = false;
		img_hideQR.enabled = false;

		float _duration = 0f;

		int _count = 0;
		foreach (var _i in _spriteArrayList) {
			_ctrl_countdown.StopAllCoroutines();
			_duration = _i.Length / _fps;
			_ctrl_countdown.PlayLoop(
				_i,
				_ctrl_countdown.img,
				_fps 
			);
			yield return new WaitForSeconds( _duration );
			if (_count++ == 0 ) {

				yield return new WaitForSeconds( 0.5f );

				ctrl_countdown.img.enabled = false;
				img_frame.enabled = true;
				ctrl_capture.Capture ();

				yield return null;
				yield return null;

				print ("url: " + url_upload);
				print ("w: " + ctrl_capture.GetTexture2D ().width);
				print ("h: " + ctrl_capture.GetTexture2D ().height);

				UploadImage.Instance.Upload(
					new WWWForm (),
					upload_filename ,
					"sampleFile",
					ctrl_capture.GetTexture2D(),
					url_upload,
					(bool _success) => {
						print( "update result: " + _success );
					}
				);

				ctrl_countdown.img.enabled = true;
				img_qr.enabled = true;
				img_hideQR.enabled = true;
				img_frame.enabled = false;
				img_preview.enabled = true;
			}
		}

		_ctrl_countdown.StopAllCoroutines();
		_ctrl_countdown.PlayLoop(
			_spriteArrayList[2],
			_ctrl_countdown.img,
			_fps 
		);
		yield return new WaitForSeconds( delay_after );

		ctrl_countdown.img.enabled = false;
		img_qr.enabled = false;
		img_frame.enabled = false;
		img_preview.enabled = false;
		img_hideQR.enabled = false;

		PlaySeq ();
	}
}
