﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class CTRL_LoadQRImage : MonoBehaviour {

	public RawImage img;

	// Use this for initialization
	void Start () {
		Texture2D tex = new Texture2D(2, 2);
		tex.LoadImage(System.IO.File.ReadAllBytes ( Application.dataPath + "/"+ "qr.png"));
		img.texture = tex;
	}
}
