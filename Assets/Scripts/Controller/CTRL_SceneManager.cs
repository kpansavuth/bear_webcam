﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BonnieSDK;

public class CTRL_SceneManager : MonoBehaviour {

	void Update() {

		if (Input.GetKeyDown (KeyCode.R)) {

			ReloadScene.Instance.ChangeScene ("0 - Test");

		}
		if (Input.GetKeyDown (KeyCode.Space)) {

			ReloadScene.Instance.ChangeScene ("1 - NoAnim");

		}
		if (Input.GetKeyDown (KeyCode.C)) {

			ReloadScene.Instance.ChangeScene ("2 - BearConfig");

		}

	}

}
