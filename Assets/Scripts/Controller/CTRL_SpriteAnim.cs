﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BonnieSDK;
using UnityEngine.UI;

public class CTRL_SpriteAnim : MonoBehaviour {

	public void PlayLoop (string _path, Image _img, float _fps)
	{
		Sprite[] _sprites = SpriteLoader.LoadSprite (_path);
		PlayLoop( _sprites, _img, _fps );
	}

	public void PlayLoop (Sprite[] _sprites, Image _img, float _fps)
	{
		StartCoroutine( 
			CoLoop(
				_sprites,
				_img,
				_fps
				)
		);
	}


	protected IEnumerator CoLoop (Sprite[] _sprites, Image _img, float _fps)
	{

		WaitForSeconds _wait = new WaitForSeconds (1f / _fps);
		int _index = 0;

		while (true) {

			_img.sprite = _sprites [_index];
			if ( ++_index >= _sprites.Length) {
				_index = 0;
			}

			yield return _wait;
		}

	}
}
