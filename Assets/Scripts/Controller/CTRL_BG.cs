﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BonnieSDK;
using UnityEngine.UI;

public class CTRL_BG : CTRL_SpriteAnim {

	public Image img;

	// Use this for initialization
	IEnumerator Start () {

		var _kvp = ReadConfigFile.ExtractKeyValuePair(
			ReadConfigFile.ReadConfig( "my_config.cfg" )
		);

		PlayLoop(
			_kvp[ "PATH_BG" ] ,
			img,
			float.Parse( _kvp[ "FPS_BG" ] )
		);

		yield break;
		
	}


}
