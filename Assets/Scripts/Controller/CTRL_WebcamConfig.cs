﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using BonnieSDK;

public class CTRL_WebcamConfig : MonoBehaviour {

	public RectTransform rect_tran;
	public float move_speed = 50f;
	public float scale_speed = 50f;

	// Use this for initialization
	void Start () {
		LoadConfig ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.Alpha6)) {
			Vector3 move = Vector3.zero;

			if (Input.GetKey (KeyCode.W)) {
				move.y += move_speed * Time.deltaTime * 0.5f;
			}
			if (Input.GetKey (KeyCode.S)) {
				move.y -= move_speed * Time.deltaTime * 0.5f;
			}
			if (Input.GetKey (KeyCode.D)) {
				move.x += move_speed * Time.deltaTime * 0.5f;
			}
			if (Input.GetKey (KeyCode.A)) {
				move.x -= move_speed * Time.deltaTime * 0.5f;
			}

			rect_tran.localPosition += move;

		}
		if (Input.GetKey (KeyCode.Alpha7)) {
			Vector2 _size = rect_tran.sizeDelta;


			if (Input.GetKey (KeyCode.W)) {
				_size.x += scale_speed * Time.deltaTime;
			}
			if (Input.GetKey (KeyCode.S)) {
				_size.x -= scale_speed * Time.deltaTime;
			}
			if (Input.GetKey (KeyCode.A)) {
				_size.y -= scale_speed * Time.deltaTime;
			}
			if (Input.GetKey (KeyCode.D)) {
				_size.y += scale_speed * Time.deltaTime;
			}

			rect_tran.sizeDelta = _size;

		}

		if ( 
			Input.GetKeyUp (KeyCode.Alpha6) ||
			Input.GetKeyUp (KeyCode.Alpha7)  
		)
		{
			SaveConfig ();
		}
		
	}

	private void SaveConfig() {
		var _data = new Dictionary<string,string> ();
		_data.Add ("px", rect_tran.transform.localPosition.x.ToString ("N2"));
		_data.Add ("py", rect_tran.transform.localPosition.y.ToString ("N2"));
		_data.Add ("pz", rect_tran.transform.localPosition.z.ToString ("N2"));
		_data.Add ("w", rect_tran.sizeDelta.x.ToString ("N2"));
		_data.Add ("h", rect_tran.sizeDelta.y.ToString ("N2"));

		WriteConfigFile.WriteConfig ("webcam.cfg", _data);


	}
	private void LoadConfig() {

		var _kvp = ReadConfigFile.ExtractKeyValuePair (
			ReadConfigFile.ReadConfig ("webcam.cfg")
		);
		rect_tran.transform.localPosition = new Vector3 (
			float.Parse (_kvp ["px"]),
			float.Parse (_kvp ["py"]),
			float.Parse (_kvp ["pz"])
		);

		Vector2 _size = new Vector2 (
			float.Parse (_kvp ["w"]),
			float.Parse (_kvp ["h"])
		                );
		rect_tran.sizeDelta = _size;

	}

}
