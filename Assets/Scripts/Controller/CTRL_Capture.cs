﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CTRL_Capture : MonoBehaviour {

	public RawImage img;
	public RectTransform input_tran;

	private float VIRTUAL_W = 2560f;
	private float VIRTUAL_H = 1600f;

	private bool _processing = false;
	private Texture2D cache_tex = null;
	public Texture2D GetTexture2D() {
		return cache_tex;
	}

	public void Capture() {
		_processing = true;
	}

	IEnumerator RecordFrame()
	{
		yield return new WaitForEndOfFrame();
		var _texture = ScreenCapture.CaptureScreenshotAsTexture();

/* Crop Screen
		// do something with texture
//		img.texture = _texture;

//		print ("_texture.width: " + _texture.width + ",  _texture.height:" + _texture.height);

		float _y = VIRTUAL_H * 0.5f + input_tran.localPosition.y - input_tran.rect.height * 0.5f;
//		print (input_tran.localPosition.y);
//		print (input_tran.rect.height);
//		print ("y: " + _y);

		_y *= _texture.height / VIRTUAL_H;
		float _w = 576f * _texture.width / VIRTUAL_W;
		float _h = 1024f * _texture.height / VIRTUAL_H;
//		float _w = input_tran.sizeDelta.y * _texture.width / VIRTUAL_W;
//		float _h = input_tran.sizeDelta.x * _texture.height / VIRTUAL_H;

//		print (_w);
//		print (_h);


		Color[] pixels = _texture.GetPixels (0, (int)_y,(int)_w, (int)_h);

		Texture2D _cropped = new Texture2D ( (int)_w, (int)_h, TextureFormat.RGB24, false);


		_cropped.SetPixels (0, 0, _cropped.width, _cropped.height, pixels, 0);

*/

		/// Full Screen
		int widthCrop = 854;
		Color[] pixels = _texture.GetPixels (0,0, widthCrop, _texture.height);
		Texture2D _cropped = new Texture2D ( widthCrop, _texture.height, TextureFormat.RGB24, false);
		_cropped.SetPixels (0, 0, _cropped.width, _cropped.height, pixels, 0);

		///

		_cropped.Apply ();
		if (cache_tex != null) {
			Object.Destroy(cache_tex);
		}
		cache_tex = _cropped;
		img.texture = cache_tex;

		//cleanup
		Object.Destroy(_texture);
	}

	void LateUpdate()
	{
		if (_processing) { 
			_processing = false;
			StartCoroutine (RecordFrame ());
		}
	}

}
