﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BonnieSDK;

public class CTRL_BearAdjust : MonoBehaviour {

	public Transform base_hor;
	public Transform base_ver;
	public Transform tran;
	public float move_speed = 5f;
	public float rot_speed = 30f;
	public float scale_speed = 1f;

	public Animator m_animator;

	// Use this for initialization
	void Start () {
		LoadConfig ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.Alpha1)) {
			Vector3 move = Vector3.zero;

			if (Input.GetKey (KeyCode.W)) {
				move.z += move_speed * Time.deltaTime * 0.5f;
			}
			if (Input.GetKey (KeyCode.S)) {
				move.z -= move_speed * Time.deltaTime * 0.5f;
			}
			if (Input.GetKey (KeyCode.D)) {
				move.x += move_speed * Time.deltaTime * 0.5f;
			}
			if (Input.GetKey (KeyCode.A)) {
				move.x -= move_speed * Time.deltaTime * 0.5f;
			}
			if (Input.GetKey (KeyCode.Q)) {
				move.y += move_speed * Time.deltaTime * 0.5f;
			}
			if (Input.GetKey (KeyCode.E)) {
				move.y -= move_speed * Time.deltaTime * 0.5f;
			}

			base_hor.position += move;
		}
		if (Input.GetKey (KeyCode.Alpha2)) {
			if (Input.GetKey (KeyCode.W)) {
				base_ver.Rotate ( 0f, 0f, rot_speed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.S)) {
				base_ver.Rotate ( 0f, 0f, -rot_speed * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.A)) {
				base_hor.Rotate ( 0f, rot_speed * Time.deltaTime ,  0f);
			}
			if (Input.GetKey (KeyCode.D)) {
				base_hor.Rotate ( 0f, -rot_speed * Time.deltaTime , 0f);
			}
		}
		if (Input.GetKey (KeyCode.Alpha3)) {
			if (Input.GetKey (KeyCode.W)) {
				float _scale = tran.localScale.x;
				_scale += scale_speed * Time.deltaTime;
				tran.localScale = Vector3.one * _scale;
			}
			if (Input.GetKey (KeyCode.S)) {
				float _scale = tran.localScale.x;
				_scale -= scale_speed * Time.deltaTime;
				tran.localScale = Vector3.one * _scale;
			}
		}

		if ( 
			Input.GetKeyUp (KeyCode.Alpha1) ||
			Input.GetKeyUp (KeyCode.Alpha2) ||
			Input.GetKeyUp (KeyCode.Alpha3)) 
		{
			SaveConfig ();
		}
		
	}

	private void SaveConfig() {
		var _data = new Dictionary<string,string> ();
		_data.Add ("px", base_hor.transform.position.x.ToString ("N2"));
		_data.Add ("py", base_hor.transform.position.y.ToString ("N2"));
		_data.Add ("pz", base_hor.transform.position.z.ToString ("N2"));
		_data.Add ("rx", "0");
		_data.Add ("ry", base_hor.localEulerAngles.y.ToString ("N2"));
		_data.Add ("rz", base_ver.localEulerAngles.z.ToString ("N2"));
		_data.Add ("scale", tran.localScale.x.ToString ("N2") );

		WriteConfigFile.WriteConfig ("bear.cfg", _data);


	}
	private void LoadConfig() {

		var _kvp = ReadConfigFile.ExtractKeyValuePair (
           	ReadConfigFile.ReadConfig ("bear.cfg")
       	);
		base_hor.position = new Vector3 (
			               float.Parse (_kvp ["px"]),
			               float.Parse (_kvp ["py"]),
			               float.Parse (_kvp ["pz"])
		               );
		base_hor.localEulerAngles = Vector3.up * float.Parse (_kvp ["ry"]);
		base_ver.localEulerAngles = Vector3.forward * float.Parse (_kvp ["rz"]);

		tran.localScale = Vector3.one * float.Parse (_kvp ["scale"]);

	}

}
