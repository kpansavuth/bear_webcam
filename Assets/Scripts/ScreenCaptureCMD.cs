﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCaptureCMD : Singleton<ScreenCaptureCMD> {

	public GameObject []showObjs;
	public GameObject []hideObjs;

	public string filename = "capture";

	public void Capture ()
	{
		StopAllCoroutines();
		StartCoroutine( CoCapture() );
		
	}

	private IEnumerator CoCapture ()
	{
		foreach (var _i in showObjs) {
			_i.SetActive( true );
		}
		foreach (var _i in hideObjs) {
			_i.SetActive( false );
		}

		ScreenCapture.CaptureScreenshot( filename );

		yield return new WaitForSeconds( 1f );

		foreach (var _i in showObjs) {
			_i.SetActive( false );
		}
		foreach (var _i in hideObjs) {
			_i.SetActive( true );
		}
	}
}
