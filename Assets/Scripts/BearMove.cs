﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearMove : MonoBehaviour {

	public float speed = 5f;
	public Transform tran;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 move = Vector3.zero;
		move.x += Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
		move.z += Input.GetAxis ("Vertical") * speed * Time.deltaTime;

		if (Input.GetKey (KeyCode.Q)) {
			move.y += speed * Time.deltaTime * 0.5f;
		}
		if (Input.GetKey (KeyCode.E)) {
			move.y -= speed * Time.deltaTime * 0.5f;
		}

		tran.position += move;
	}
}
