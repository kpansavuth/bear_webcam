﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SocketIO;
using System;
using System.Text;

namespace BonnieMMO
{

    public class JsonHelper
    {
        public static T[] getJsonArray<T>(string json)
        {
            string newJson = "{ \"array\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.array;
        }
    
        [System.Serializable]
        private class Wrapper<T>
        {
            public T[] array;
        }
    }

    public class WSBase : MonoBehaviour
    {

        [System.Serializable]
        public class ID_JSONInfo
        {
            public string _id;
        }

        public delegate void CallbackDelegate();

        public GameObject socketParent;
        protected SocketIOComponent mSocket;
        public SocketIOComponent socket
        {

            get
            {

                if (mSocket == null)
                {

                    mSocket = socketParent.GetComponentInChildren<SocketIOComponent>(false);

                }
                return mSocket;

            }

        }

        public void OnAnything(SocketIOEvent e)
        {
            Debug_Print("[SocketIO] received: " + e.name + " " + e.data);
        }

        public IEnumerator CoInterval(CallbackDelegate callback, float duration)
        {
            WaitForSeconds _wait = new WaitForSeconds(duration);
            while (true)
            {
                yield return _wait;
                if (callback != null)
                    callback(); 
                
            }
        }

        public IEnumerator CoInterval(CallbackDelegate callback)
        {
            while (true)
            {
                yield return null;
                if (callback != null)
                    callback();

            }
        }

        public virtual void Debug_Print(string txt)
        {

            print(txt);

        }

        public static string sha256(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        public static string stringify(Dictionary<string, string> _dict)
        {
            return new JSONObject(_dict).ToString().Replace("\"", "^");
        }

        public static string RemoveBackslash(string str)
        {
            return str.Replace("\\", string.Empty);

            //var charsToRemove = new string[] { "\\" };
            //foreach (var c in charsToRemove) {
            //    str = str.Replace (c, string.Empty);
            //}
        }

        public static Dictionary<string, string> authenDataDict(string userid, string auth_token)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();

            data["userid"] = userid;
            data["auth_token"] = auth_token;
            return data;
        }

        public static void addPosRotToDict(Dictionary<string, string> _dict, Vector3 pos, Vector3 rot)
        {
            _dict["px"] = pos.x.ToString("N2");
            _dict["py"] = pos.y.ToString("N2");
            _dict["pz"] = pos.z.ToString("N2");
            _dict["rx"] = rot.x.ToString("N2");
            _dict["ry"] = rot.y.ToString("N2");
            _dict["rz"] = rot.z.ToString("N2");
        }

        public static Vector3 GetPosFromJSONData(JSONObject data)
        {
            Vector3 pos = Vector3.zero;
            pos.x = float.Parse(data["px"].str);
            pos.y = float.Parse(data["py"].str);
            pos.z = float.Parse(data["pz"].str);
            return pos;
        }
        public static Vector3 GetRotFromJSONData(JSONObject data)
        {
            Vector3 rot = Vector3.zero;
            rot.x = float.Parse(data["rx"].str);
            rot.y = float.Parse(data["ry"].str);
            rot.z = float.Parse(data["rz"].str);
            return rot;
        }


        public static Int32 GetTimeStamp()
        {
            return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }

}
