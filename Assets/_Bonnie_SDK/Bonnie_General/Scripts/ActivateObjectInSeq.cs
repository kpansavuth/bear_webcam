﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObjectInSeq : MonoBehaviour {


    public List<GameObject> objList = new List<GameObject>();
    public float interval = 0.5f;

	// Use this for initialization
	IEnumerator Start () {

        for (int i = 0; i < objList.Count; ++i)
        {
            objList[i].SetActive(true);
            yield return new WaitForSeconds(interval);
        }
		
	}
}
