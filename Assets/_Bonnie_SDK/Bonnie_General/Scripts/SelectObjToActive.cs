﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectObjToActive : MonoBehaviour {

	public GameObject []objList = new GameObject[]{};

	public int awakeIndex = 0;

	private int mIndex = -1;
	public void SetIndex( int _i ) {
		mIndex = Mathf.Clamp( _i, -1, objList.Length-1 );

		for(int i=0; i<objList.Length; ++i) {

			objList[i].SetActive( i == mIndex );

		}

	}

	public int GetIndex( ) {

		return mIndex;

	}

	// Use this for initialization
	void Awake () {
		SetIndex( awakeIndex );
	}

}
