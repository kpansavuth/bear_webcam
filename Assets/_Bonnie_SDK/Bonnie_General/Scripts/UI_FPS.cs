﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_FPS : MonoBehaviour {

	public Text label;

	private float fps = 0.0f;

	void Start ()
	{
		StartCoroutine(UpdateFps());
	}

	private IEnumerator UpdateFps ()
	{
		while( true ){
			fps = 1f/Time.deltaTime;
			label.text = string.Format("{0} FPS", (int)fps);
			if( fps > 50f ){
				label.color = Color.green;
			}else if( fps > 30f ){
				label.color = Color.yellow;
			}else{
				label.color = Color.red;
			}
			yield return new WaitForSeconds(1.0f);
		}
	}

}
