﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetachFromParent : MonoBehaviour {

	// Use this for initialization
	void Start () {

        this.transform.parent = null;
		
	}
	
}
