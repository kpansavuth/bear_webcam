﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeThisChildOfSomething : MonoBehaviour {

    public Transform parentTran;
    public Transform tran;
    

	void Awake () {

        if (parentTran != null)
        {
            Vector3 pos = tran.localPosition;
            Quaternion rot = tran.localRotation;
            
            tran.parent = parentTran;
            tran.localPosition = pos;
            tran.localRotation = rot;

        }
		
	}
	
}
