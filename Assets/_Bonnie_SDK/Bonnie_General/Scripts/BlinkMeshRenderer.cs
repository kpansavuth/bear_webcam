﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkMeshRenderer : MonoBehaviour {

	public MeshRenderer _renderer;
	public float showInterval = 0.5f;
	public float hideInterval = 0.5f;

	private IEnumerator CoBlink(){
		while( true ){
			_renderer.enabled = true;
			yield return new WaitForSeconds( showInterval );
			_renderer.enabled = false;
			yield return new WaitForSeconds( hideInterval );
		}		
	}	


	// Use this for initialization
	void OnEnable () {
		StartCoroutine( CoBlink() );
	}

	void OnDisable() {

		StopAllCoroutines();

	}

}
