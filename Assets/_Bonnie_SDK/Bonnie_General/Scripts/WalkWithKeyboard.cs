﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkWithKeyboard : MonoBehaviour {

    public Transform tran;
    public Transform locator;
    public float walkSpeed = 5f;
    public float turnSpeed = 100f;
    
    public enum WALK_MODE {
    
        SIDE_STEP = 0,
        TURN_AROUND = 1,
    
    }
    public WALK_MODE walkMode = WALK_MODE.SIDE_STEP;
	
	// Update is called once per frame
	void Update () {

        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");
        Vector3 direction = locator.forward;

        direction.y = 0f;
        tran.position += direction * (v * walkSpeed * Time.deltaTime);
        
        if( walkMode == WALK_MODE.SIDE_STEP ) {
        
            direction = locator.right;
            direction.y = 0f;
            tran.position += direction * (h * walkSpeed * Time.deltaTime);
        
        }else if( walkMode == WALK_MODE.TURN_AROUND ) {
            
            tran.Rotate(0f, h * turnSpeed * Time.deltaTime, 0f);
        
        }        
		
	}
}
