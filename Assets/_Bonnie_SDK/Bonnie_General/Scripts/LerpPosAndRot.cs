﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpPosAndRot : MonoBehaviour {

	public Transform inputTran;
	public Transform outputTran;
	public float dampingPos = 1f;
	public float dampingRot = 1f;

    public bool useLateUpdate = false;
    public bool applyLocalCoordinate = false;
	
	// Update is called once per frame
	void Update ()
	{

		if (useLateUpdate) {

			return;
        
		}

		if (applyLocalCoordinate) {
			outputTran.localPosition = Vector3.LerpUnclamped (outputTran.localPosition, inputTran.position, dampingPos * Time.deltaTime);
			outputTran.localRotation = Quaternion.LerpUnclamped (outputTran.localRotation, inputTran.rotation, dampingRot * Time.deltaTime);
		} else {
			outputTran.position = Vector3.LerpUnclamped (outputTran.position, inputTran.position, dampingPos * Time.deltaTime);
			outputTran.rotation = Quaternion.LerpUnclamped (outputTran.rotation, inputTran.rotation, dampingRot * Time.deltaTime);
		}
		
	}
    
    void LateUpdate ()
	{

		if (!useLateUpdate) {

			return;
        
		}
		if (applyLocalCoordinate) {
			outputTran.localPosition = Vector3.LerpUnclamped (outputTran.localPosition, inputTran.position, dampingPos * Time.deltaTime);
			outputTran.localRotation = Quaternion.LerpUnclamped (outputTran.localRotation, inputTran.rotation, dampingRot * Time.deltaTime);
		} else {
			outputTran.position = Vector3.LerpUnclamped (outputTran.position, inputTran.position, dampingPos * Time.deltaTime);
			outputTran.rotation = Quaternion.LerpUnclamped (outputTran.rotation, inputTran.rotation, dampingRot * Time.deltaTime);
		}
        
    }
}
