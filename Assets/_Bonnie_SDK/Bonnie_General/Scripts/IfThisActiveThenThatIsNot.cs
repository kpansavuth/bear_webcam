﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IfThisActiveThenThatIsNot : MonoBehaviour {

	public GameObject thisObj;
	public GameObject thatObj;

	void Update () {

		thatObj.SetActive( !thisObj.activeInHierarchy );

	}
}
