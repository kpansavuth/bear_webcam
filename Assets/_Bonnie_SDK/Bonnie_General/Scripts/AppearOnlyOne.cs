﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearOnlyOne : MonoBehaviour {

	public GameObject []objs;

	private void Check ()
	{

		objs [1].SetActive ( !objs [0].activeInHierarchy );

	}

	// Use this for initialization
	void FixedUpdate () {

		Check();
		
	}

}
