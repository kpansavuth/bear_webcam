﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AppVersion : MonoBehaviour {

    public Text label;

	// Use this for initialization
	void Start () {
		label.text = "Application Version : " + Application.version;
	}
}
