using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class TimerAlarm : MonoBehaviour {
	
	
	private static TimerAlarm instanceOfTimerAlarm;
	 public static TimerAlarm Shared{
	  get{
	   if( instanceOfTimerAlarm == null){
	    GameObject go = new GameObject("Timer Alarm");
	    instanceOfTimerAlarm = (TimerAlarm)go.AddComponent<TimerAlarm>();
	    instanceOfTimerAlarm.SetupDelegate();
	   }
	   return instanceOfTimerAlarm;
	  }
	 }

	public delegate void UpdateDelegate();
	public UpdateDelegate updateDelegate = null;

	private List<TimerObj> timerObjs = new List<TimerObj>();
	public void AddTimerObj(TimerObj node){
		SetupDelegate();
		timerObjs.Add(node);
	}
	
	private List<TimerObj> timerObjToDelete = new List<TimerObj>();
	private void UpdateTimerObjs(){
		timerObjToDelete.Clear();		
		for(int i=0;i<timerObjs.Count; ++i){
			timerObjs[i].UpdateTimer();	
			if(timerObjs[i].isExpire){
				timerObjToDelete.Add(timerObjs[i]);
			}
		}
		
		for(int i=0;i<timerObjToDelete.Count; ++i){
			timerObjs.Remove( timerObjToDelete[i] );
		}

		if(timerObjs.Count <= 0){ updateDelegate = null; }
	}
	
	public static TimerObj LazyTimer(float duration,TimerObj.FinishTimerDelegate _finishTimerDelegate,bool isIgnoreTimerScale = false){

		if(isIgnoreTimerScale){
			TimerObj timer = new TimerObj(duration,_finishTimerDelegate);	
			timer.isIgnoreTimeScale = true;
			return timer;
		}
		return new TimerObj(duration,_finishTimerDelegate);	
	}
	
	public static TimerObj LazyScheduleTimer(float duration,TimerObj.FinishTimerDelegate _finishTimerDelegate,bool isIgnoreTimerScale = false){
	
		TimerObj timer = new TimerObj(duration,_finishTimerDelegate);
		timer.isSchedule = true;
		timer.isIgnoreTimeScale = isIgnoreTimerScale;
		return timer;
	}
	
	private  void SetupDelegate(){
		if(updateDelegate == null){
			updateDelegate = UpdateTimerObjs;
		}
	}
	
	protected void Awake(){
		SetupDelegate();
	}

	void Update(){
		if( updateDelegate != null ) updateDelegate();
	}
}

public class TimerObj{
	
	public TimerObj(float _duration){
		duration = _duration;
		TimerAlarm.Shared.AddTimerObj(this);
	}
	
	public TimerObj(float _duration,FinishTimerDelegate _finishTimerDelegate){		
		finishTimerDelegate = _finishTimerDelegate;
		duration = _duration;
		TimerAlarm.Shared.AddTimerObj(this);
	}

	public void ForceEndTimer(){
		timePos = 0f;		
		if(finishTimerDelegate != null){
			finishTimerDelegate();					
		}		
		StopTimer();
	}
	
	public void StopTimer(){
		isExpire = true;	
	}
		
	private float timePos = 0;
	private float duration = 0;
	
	public delegate void FinishTimerDelegate();
	public FinishTimerDelegate finishTimerDelegate;
	public bool isIgnoreTimeScale = false;
	
	public bool isSchedule = false;

	public bool isExpire = false;
	
	public void UpdateTimer(){
		if(isExpire){return;}
		timePos += Time.deltaTime;
		if(timePos >= duration){
			timePos = 0f;
			
			if(finishTimerDelegate != null){
				finishTimerDelegate();					
			}
			
			if(!isSchedule){
				isExpire = true;
			}
		}
	}		
}

public class ClockGroup{
	public void AddClock( TimerObj clock ){
		clockList.Add(clock);
	}

	private List<TimerObj> clockList = new List<TimerObj>();
	public List<TimerObj> GetClockList(){
		return clockList;
	}
	public void StopAllClock(){
		for(int i=0;i<clockList.Count;++i){
			if( clockList[i] == null ){ continue; }
			clockList[i].StopTimer();
		}
		clockList.Clear();
	}
}
