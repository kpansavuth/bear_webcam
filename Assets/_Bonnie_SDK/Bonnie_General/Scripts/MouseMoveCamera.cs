using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMoveCamera : MonoBehaviour {

	public Transform baseHor;
	public Transform baseVer;

	private Vector3 mouseBeginPos = Vector3.zero;

	public float sensitivity = 100f; 
	private Vector3 diff = Vector3.zero;

	// Use this for initialization
	void Start () {

		mouseBeginPos = Input.mousePosition;
		
	}

#if UNITY_EDITOR

    // Update is called once per frame
    void Update () {

		diff = Input.mousePosition - mouseBeginPos;
		baseHor.localEulerAngles = Vector3.up * ( diff.x * sensitivity / Screen.width );
		baseVer.localEulerAngles = -Vector3.right * ( diff.y * sensitivity  / Screen.height );
        
        if( Input.GetKeyDown("r") ) {
        
            mouseBeginPos = Input.mousePosition;
        
        }
		
	}



    private void OnGUI()
	{
        GUILayout.Label("Press [R] to reset mouse position");
	}
    
  #endif
}
